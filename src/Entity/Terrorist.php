<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 10/02/2019
 * Time: 21:20
 */

namespace App\Entity;

use Swagger\Annotations as SWG;

/**
 * Class Terrorist
 * @package App\Entity
 * @SWG\Definition()
 */
class Terrorist
{
    /**
     * @SWG\Property(
     *     property="fullName",
     *     type="string",
     *     example="Рахат Лукум"
     * )
     */
    private $fullName;
    /**
     * @SWG\Property(
     *     property="altName",
     *     type="string",
     *     example="Рахат-Лукум, Раххат-Лукум",
     *     x={"nullable": true}
     * )
     */
    private $altName;
    /**
     * @SWG\Property(
     *     property="birthDate",
     *     type="string",
     *     format="date",
     *     example="1990-11-30",
     *     x={"nullable": true}
     * )
     */
    private $birthDate;
    /**
     * @SWG\Property(
     *     property="listNumber",
     *     type="integer",
     *     example=1
     * )
     */
    private $listNumber;
    /**
     * @SWG\Property(
     *     property="address",
     *     type="string",
     *     example="г. Н",
     *     x={"nullable": true}
     * )
     */
    private $address;
    /**
     * @SWG\Property(
     *     property="originalItem",
     *     type="string",
     *     example="1. Рахат-Лукум*, (Рахат-Лукум; Раххат-Лукум), 30.11.1990 г.р. , г. Н"
     * )
     */
    private $originalItem;

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     * @return Terrorist
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAltName()
    {
        return $this->altName;
    }

    /**
     * @param mixed $altName
     * @return Terrorist
     */
    public function setAltName($altName)
    {
        $this->altName = $altName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param mixed $birthDate
     * @return Terrorist
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getListNumber()
    {
        return $this->listNumber;
    }

    /**
     * @param mixed $listNumber
     * @return Terrorist
     */
    public function setListNumber($listNumber)
    {
        $this->listNumber = $listNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return Terrorist
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOriginalItem()
    {
        return $this->originalItem;
    }

    /**
     * @param mixed $originalItem
     * @return Terrorist
     */
    public function setOriginalItem($originalItem)
    {
        $this->originalItem = $originalItem;
        return $this;
    }
}