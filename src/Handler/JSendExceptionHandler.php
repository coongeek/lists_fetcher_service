<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 11/02/2019
 * Time: 21:35
 */

namespace App\Handler;


use FOS\RestBundle\Exception\InvalidParameterException;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonSerializationVisitor;
use JMS\Serializer\Context;


class JSendExceptionHandler implements SubscribingHandlerInterface
{
    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => \Exception::class,
                'method' => 'serializeExceptionToJson'
            ]
        ];
    }

    public function serializeExceptionToJson(
        JsonSerializationVisitor $visitor,
        \Exception $exception,
        array $type,
        Context $context
    ) {
        if ($exception instanceof InvalidParameterException) {
            $data = [
                'status' => 'fail',
                'data' => [
                    $exception->getParameter()->getName() => $exception->getMessage()
                ]
            ];
        } else {
            $data = [
                'status' => 'error',
                'message' => 'Unexpected application error.'
            ];
        }

        return $visitor->visitArray($data, $type, $context);
    }
}