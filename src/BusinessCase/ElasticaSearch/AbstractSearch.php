<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 16/01/2019
 * Time: 15:46
 */

namespace App\BusinessCase\ElasticaSearch;

use App\BusinessCase\TerroristIndexManager\TerroristIndexManagers\ElasticTerroristIndexManager;
use Elastica\Client as ElasticaClient;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractSearch
{
    protected $elasticaClient;
    protected $pageSize;

    protected $lastSearchPage;
    /** @var \Elastica\ResultSet */
    protected $lastSearchResult;

    public function __construct(ElasticaClient $elasticaClient, int $pageSize = 25)
    {
        $this->elasticaClient = $elasticaClient;
        $this->pageSize = $pageSize;
    }

    abstract function search(array $params);

    /**
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function getLastSearchPaging(Request $request)
    {
        if (is_null($this->lastSearchPage) || empty($this->lastSearchResult)) {
            throw new \Exception('Should run search first.');
        }

        $paging = [];
        $page = $this->lastSearchPage;
        $resultSet = $this->lastSearchResult;
        $size = $this->pageSize;
        $listItems = $resultSet->getResults();

        if (!empty($listItems) && $page > 0) {
            $previousPage = $page - 1;

            $host = $request->getHost();
            $uri = $request->getRequestUri();

            $url = "{$host}{$uri}";

            if (preg_match('#page=[1-9]+#', $url) === 1) {
                $url = preg_replace('#page=[1-9]+#', "page={$previousPage}", $url);
            } else {
                $query = parse_url($url, PHP_URL_QUERY);

                if ($query) {
                    $url .= "&page={$previousPage}";
                } else {
                    $url .= "?page={$previousPage}";
                }
            }

            $paging['previous'] = $url;
        }

        if (!empty($listItems) && $page * $size + count($listItems) < $resultSet->getTotalHits()) {
            $nextPage = $page + 1;

            $host = $request->getHost();
            $uri = $request->getRequestUri();

            $url = "{$host}{$uri}";

            if (preg_match('#page=[1-9]+#', $url) === 1) {
                $url = preg_replace('#page=[1-9]+#', "page={$nextPage}", $url);
            } else {
                $query = parse_url($url, PHP_URL_QUERY);

                if ($query) {
                    $url .= "&page={$nextPage}";
                } else {
                    $url .= "?page={$nextPage}";
                }
            }

            $paging['next'] = $url;
        }

        return $paging;
    }
}