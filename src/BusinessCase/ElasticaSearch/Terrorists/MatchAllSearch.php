<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 16/01/2019
 * Time: 14:20
 */

namespace App\BusinessCase\ElasticaSearch\Terrorists;

use App\BusinessCase\ActualTerroristsIndex;
use App\BusinessCase\ElasticaSearch\AbstractSearch;
use App\BusinessCase\ParameterException;
use App\BusinessCase\TerroristsSearches\MatchAllSearchInterface;
use Elastica\Client as ElasticaClient;
use Elastica\Search as ElasticaSearch;
use Elastica\Query as ElasticaQuery;

/**
 * Class MatchAllSearch
 * @package App\BusinessCase\ElasticaSearch\Terrorists
 */
class MatchAllSearch extends AbstractSearch implements MatchAllSearchInterface
{
    protected $actualTerroristsIndex;

    public function __construct(
        ElasticaClient $elasticaClient,
        ActualTerroristsIndex $actualTerroristsIndex,
        int $pageSize = 25
    ) {
        $this->actualTerroristsIndex = $actualTerroristsIndex;
        parent::__construct($elasticaClient, $pageSize);
    }

    /**
     * @param array $params
     * @return \Elastica\ResultSet
     * @throws \Exception
     */
    public function search(array $params)
    {
        if (!array_key_exists('page', $params)) {
            $page = 0;
        } else {
            $page = $params['page'];

            if ($page !== 0 && $page !== '0' && intval($page) === 0) {
                throw new ParameterException('page', 'Invalid parameter type: \'page\' must be integer.');
            }
        }

        $search = new ElasticaSearch($this->elasticaClient);

        $actualTerroristIndexName = $this->actualTerroristsIndex->getName();
        $search->addIndex($actualTerroristIndexName);
        if (!is_null($this->actualTerroristsIndex->getType())) {
            $search->addType($this->actualTerroristsIndex->getType());
        }

        $query = new ElasticaQuery([
            'from' => $page * $this->pageSize,
            'size' => $this->pageSize,
            'query' => [
                'match_all' => (object)[]
            ],
            'sort' => [
                'listNumber' => ["order" => "asc", "unmapped_type" =>  "integer"]
            ]
        ]);

        $search->setQuery($query);
        $resultSet = $search->search();

        $results = $resultSet->getResults();

        $terrorists = [];

        foreach ($results as $result) {
            $terrorists[] = $result->getSource();
        }

        $this->lastSearchPage = $page;
        $this->lastSearchResult = $resultSet;

        return $terrorists;
    }
}