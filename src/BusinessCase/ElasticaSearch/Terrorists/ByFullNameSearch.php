<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 16/01/2019
 * Time: 15:56
 */

namespace App\BusinessCase\ElasticaSearch\Terrorists;


use App\BusinessCase\ActualTerroristsIndex;
use App\BusinessCase\ElasticaSearch\AbstractSearch;
use App\BusinessCase\Terrorist;
use App\BusinessCase\TerroristsSearches\ByFullNameSearchInterface;
use Elastica\Client as ElasticaClient;
use Elastica\Search as ElasticaSearch;
use Elastica\Query as ElasticaQuery;

/**
 * Class ByFullNameSearch
 * @package App\BusinessCase\ElasticaSearch\Terrorists
 */
class ByFullNameSearch extends AbstractSearch implements ByFullNameSearchInterface
{
    protected $actualTerroristsIndex;

    public function __construct(
        ElasticaClient $elasticaClient,
        ActualTerroristsIndex $actualTerroristsIndex,
        int $pageSize = 25)
    {
        $this->actualTerroristsIndex = $actualTerroristsIndex;

        parent::__construct($elasticaClient, $pageSize);
    }

    /**
     * @param array $params
     * @return array|mixed
     * @throws \Exception
     */
    public function search(array $params)
    {
        if (!array_key_exists('page', $params)) {
            $page = 0;
        } else {
            $page = $params['page'];

            if ($page !== 0 && $page !== '0' && intval($page) === 0) {
                $val = (string)$page;
                $errorMsg = "Invalid 'page' parameter value: {$val}."
                            . " Only integer values are supported.";

                throw new \Exception($errorMsg);
            }
        }

        if (array_key_exists('fuzziness', $params)) {
            $fuzziness = $params['fuzziness'];

            if (!in_array($fuzziness, [0, 1, 2])) {
                $val = (string)$fuzziness;
                $errorMsg = "Invalid 'fuzziness' parameter value: {$val}."
                            . ' Supported values: 0(default), 1, 2.';

                throw new \Exception($errorMsg);
            }
        } else {
            $fuzziness = 0;
        }

        if (!array_key_exists('fullName', $params)
            || preg_match(Terrorist::FULL_NAME_PATTERN, $params['fullName']) !== 1) {
            $val = $params['fullName'];
            $errorMsg = "Invalid 'fullName' parameter value: {$val}"
                        . " Only values matching following pattern are supported: "
                        . Terrorist::FULL_NAME_PATTERN;
            throw new \Exception($errorMsg);
        } else {
            $fullName = $params['fullName'];
        }

        $search = new ElasticaSearch($this->elasticaClient);
        $search->addIndex($this->actualTerroristsIndex->getName());

        if (mb_strlen($this->actualTerroristsIndex->getType()) > 0) {
            $search->addType($this->actualTerroristsIndex->getType());
        }

        $query = new ElasticaQuery([
            'from' => $page * $this->pageSize,
            'size' => $this->pageSize,
            'query' => [
                'multi_match' => [
                    'query' => $fullName,
                    'operator' => 'and',
                    'fields' => ['fullName', 'altName'],
                    'fuzziness' => $fuzziness
                ],
            ],
        ]);

        $search->setQuery($query);
        $resultSet = $search->search();

        $results = $resultSet->getResults();
        $terrorists = [];

        foreach ($results as $result) {
            $terrorists[] = $result->getSource();
        }

        $this->lastSearchPage = $page;
        $this->lastSearchResult = $resultSet;

        return $terrorists;
    }
}