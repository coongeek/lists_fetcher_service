<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 16/01/2019
 * Time: 22:32
 */

namespace App\BusinessCase\ElasticaSearch\Terrorists;


use App\BusinessCase\ActualTerroristsIndex;
use App\BusinessCase\ElasticaSearch\AbstractSearch;
use App\BusinessCase\TerroristsSearches\ByIdSearchInterface;
use Elastica\Client as ElasticaClient;
use Elastica\Search as ElasticaSearch;
use Elastica\Query as ElasticaQuery;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ByIdSearch
 * @package App\BusinessCase\ElasticaSearch\Terrorists
 */
class ByIdSearch extends AbstractSearch implements ByIdSearchInterface
{
    protected $actualTerroristsIndex;

    public function __construct(
        ElasticaClient $elasticaClient,
        ActualTerroristsIndex $actualTerroristsIndex,
        int $pageSize = 25)
    {
        $this->actualTerroristsIndex = $actualTerroristsIndex;
        parent::__construct($elasticaClient, $pageSize);
    }

    /**
     * @param array $params
     * @return array|bool|mixed
     * @throws \Exception
     */
    public function search(array $params)
    {
        if (!array_key_exists('id', $params)
            || !preg_match('#^\d+$#', $params['id']) == 1) {
            throw new \Exception('Invalid id parameter type.');
        }

        $id = $params['id'];

        $search = new ElasticaSearch($this->elasticaClient);
        $search->addIndex($this->actualTerroristsIndex->getName());

        if (!empty($this->actualTerroristsIndex->getType())) {
            $search->addType($this->actualTerroristsIndex->getType());
        }

        $query = new ElasticaQuery([
            'query' => [
                'term' => [
                    'listNumber' => $id,
                ]
            ]
        ]);

        $search->setQuery($query);
        $resultSet = $search->search();

        $results = $resultSet->getResults();

        if (empty($results)) {
            return false;
        } else {
            return $results[0]->getSource();
        }
    }

    public function getLastSearchPaging(Request $request)
    {
        throw new \Exception('Paging is not supported for this query.');
    }
}