<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 10/01/2019
 * Time: 21:46
 */

namespace App\BusinessCase\TerroristListDOMLoader\Exception;

/**
 * Class TerroristListDOMLoaderException
 *
 * @package App\BusinessCase\TerroristListDOMLoader\Exception
 * @author Valentin Kniazev <coongeek@gmail.com>
 */
class TerroristListDOMLoaderException extends \Exception {}