<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 10/01/2019
 * Time: 22:23
 */

namespace App\BusinessCase\TerroristListDOMLoader\Event;


use Symfony\Component\EventDispatcher\Event;

/**
 * Class LoaderErrorEvent signals about error
 * occurred during loading of terrorist list dom.
 *
 * @package App\BusinessCase\TerroristListDOMLoader\Event
 */
class LoaderErrorEvent extends Event
{
    const NAME = 'terrorist_list.loader_error';

    protected $error;

    public function __construct(\Exception $ex)
    {
        $this->error = $ex;
    }

    public function getError()
    {
        return $this->error;
    }
}