<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 06/01/2019
 * Time: 10:59
 */

namespace App\BusinessCase\TerroristListDOMLoader;


use App\BusinessCase\Foundation\DOMParserResultProcessor\DOMLoaderInterface;
use App\BusinessCase\TerroristListDOMLoader\Event\LoaderErrorEvent;
use App\BusinessCase\TerroristListDOMLoader\Exception\TerroristListDOMLoaderException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class TerroristListDOMLoader
 *
 * @package App\BusinessCase\TerroristListDOMLoader
 * @author Valentin Kniazev <coongeek@gmail.com>
 */
class TerroristListDOMLoader implements DOMLoaderInterface
{
    private $cookiePath;

    public function __construct(string $cookiePath)
    {
        $this->cookiePath = $cookiePath;
    }

    /**
     * Loads DOM containing terrorist list.
     *
     * @return string
     * @throws TerroristListDOMLoaderException
     */
    public function load()
    {
        $resource = "http://www.fedsfm.ru/documents/terrorists-catalog-portal-act";
        $referer = "http://www.google.com";

        try {
            $ch = curl_init($resource);

            $tmpfname = $this->cookiePath;
            $fp = fopen($this->cookiePath,"w") or die("<BR />Unable to open cookie file for write!<BR />");
            fclose($fp);

            curl_setopt($ch, CURLOPT_COOKIEJAR, $tmpfname);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $tmpfname);

            curl_setopt($ch, CURLOPT_FAILONERROR, true);

            curl_setopt($ch, CURLOPT_REFERER, $referer);
            curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
            curl_setopt($ch, CURLOPT_ENCODING, "");
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Accept: text/html,application/xhtml+xm…ml;q=0.9,image/webp,*/*;q=0.8',
                'Accept-Encoding: gzip, deflate',
                'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
                'Connection: keep-alive',
                'Host: www.fedsfm.ru',
                'Upgrade-Insecure-Requests: 1',
                'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'
            ]);

            $dom = curl_exec($ch);

            if (curl_error($ch)) {
                $loaderException = new TerroristListDOMLoaderException(curl_error($ch));
            }

            curl_close($ch);

            if (isset($loaderException)) {
                throw $loaderException;
            }

            return $dom;
        } catch (\Exception $ex) {
            if (false === $ex instanceof TerroristListDOMLoaderException) {
                $ex = new TerroristListDOMLoaderException($ex->getMessage(), $ex->getCode(), $ex);
            }

            throw $ex;
        }
    }
}