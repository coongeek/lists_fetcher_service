<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 03/02/2019
 * Time: 18:11
 */

namespace App\BusinessCase;


use Elastica\Client;

/**
 * Class TerroristsIndex provides creation of
 * terrorist catalog indices with predefined schema.
 *
 * @package App\BusinessCase
 */
class TerroristsIndex
{
    const PREFIX = 'terrorist_list_';

    protected $prefix;
    protected $type;
    protected $elasticaClient;

    public function __construct(Client $elasticaClient, string $prefix, string $type)
    {
        $this->prefix = $prefix;
        $this->type = $type;
        $this->elasticaClient = $elasticaClient;
    }

    public function getPrefix()
    {
        return $this->prefix;
    }

    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $name
     * @param $type
     * @return \Elastica\Index
     * @throws \Exception
     */
    protected function createIndex($name, $type)
    {
        //TODO: make type nullable to provide support for latest ElasticSearch versions
        $index = $this->elasticaClient->getIndex($name);

        if ($index->exists()) {
            throw new \Exception('Attempt to create already existing index.');
        }

        try {
            $index->create([
                'number_of_shards' => 1,
                'number_of_replicas' => 1,
                'analysis' => [
                    'analyzer' => [
                        'ru_analyzer' => [
                            'type' => 'custom',
                            'tokenizer' => 'letter',
                            'filter' => [
                                'standard',
                                'uppercase',
                                'russian_morphology',
                            ],
                        ]
                    ]
                ],
            ]);

            $type = $index->getType($type);

            $mapping = new \Elastica\Type\Mapping();
            $mapping->setType($type);

            $mapping->enableAllField(false);
            $mapping->setProperties([
                'listNumber' => ['type' => 'integer', 'include_in_all' => false],
                'fullName' => ['type' => 'string'],
                'birthDate' => [
                    'type' => 'date',
                    'format' => "yyyy-MM-dd",
                    'include_in_all' => false
                ],
                'altName' => ['type' => 'string', 'include_in_all' => true],
                'original' => ['type' => 'string', 'include_in_all' => false]
            ]);

            $mapping->send();

            return $index;
        } catch (\Exception $ex) {
            if ($index->exists()) {
                $index->delete();
            }
        }
    }

    /**
     * Creates terrorist catalog index with predefined schema
     * and random name(with prefix, specified in PREFIX constant).
     * Returns created index name.
     *
     * @param array $terrorists
     * @param int $bulkSize
     * @return string
     * @throws \Exception
     */
    public function createActual(array $terrorists, $bulkSize = 25)
    {
        $newTerroristsIndexName = $this->prefix . uniqid();
        $newTerroristTypeName = $this->type;
        $index = $this->elasticaClient->getIndex($newTerroristsIndexName);

        if (!$index->exists()) {
            $index = $this->createIndex($newTerroristsIndexName, $newTerroristTypeName);
        }

        $terroristIndexType = $index->getType($newTerroristTypeName);

        if (!empty($terrorists)
            && array_key_exists(0, $terrorists)
            && array_key_exists('listNumber', $terrorists[0])) {
            $step = 0;
            $responses = [];

            foreach ($terrorists as $terrorist) {
                if (array_key_exists('birthDate', $terrorist)) {
                    $terrorist['birthDate'] = $terrorist['birthDate']->format('Y-m-d');
                }

                $terroristDocument = new \Elastica\Document($terrorist['listNumber'], $terrorist);
                $terroristIndexType->addDocument($terroristDocument);

                if ($step % $bulkSize == 0) {
                    $responses[] = $terroristIndexType->getIndex()->refresh();
                }

                $step++;
            }

            $responses[] = $terroristIndexType->getIndex()->refresh();
        } else {
            throw new \Exception('Terrorist list could not be empty.');
        }

        return $newTerroristsIndexName;
    }

    public function deleteByName($name)
    {
        $this->elasticaClient->getIndex($name)->delete();

        return true;
    }
}