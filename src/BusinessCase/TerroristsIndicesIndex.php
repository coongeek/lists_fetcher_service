<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 03/02/2019
 * Time: 18:12
 */

namespace App\BusinessCase;

use App\BusinessCase\Utils\MicroDateTime;
use Elastica\Client as ElasticaClient;
use Elastica\Document as ElasticaDocument;
use Elastica\Query;
use Elastica\Search;

/**
 * Class TerroristsIndicesIndex provides
 * access to operations on terrorists indices catalog index.
 *
 * @package App\BusinessCase
 */
class TerroristsIndicesIndex
{
    /**
     * Always keep records about top 2 indices to ensure
     * service availability during terrorist-list update.
     */
    const KEEP_TOP_N_INDICES = 2;

    protected $elasticaClient;
    protected $name;
    protected $type;

    public function __construct(
        ElasticaClient $client,
        string $name,
        string $type = null
    ) {
        $this->elasticaClient = $client;
        $this->name = $name;
        $this->type = $type;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }

    /**
     * Check whether terrorist indices catalog index
     * is already persisted in Elasticsearch server instance.
     *
     * @return bool
     */
    public function exists()
    {
        return $this->elasticaClient->getIndex($this->name)->exists();
    }

    /**
     * Creates terrorists indices catalog into Elasticsearch instance if
     * there is no index with such indexName/type combination present, otherwise
     * throws an exception.
     *
     * @return bool
     * @throws \Exception
     */
    public function create()
    {
        $index = $this->elasticaClient->getIndex($this->name);

        if ($index->exists()) {
            throw new \Exception('Terrorist indices index already exists.');
        }

        try {
            $index->create([
                'number_of_shards' => 1,
                'number_of_replicas' => 1
            ]);

            $type = $index->getType($this->type);

            $mapping = new \Elastica\Type\Mapping();

            $mapping->setType($type);
            $mapping->setProperties([
                'indexName' => ['type' => 'string'],
                'indexDate' => ['type' => 'date', 'format' => "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"]
            ]);

            $mapping->send();
        } catch (\Exception $ex) {
            if ($index->exists()) {
                $index->delete();
            }

            throw $ex;
        }

        return true;
    }

    /**
     * Adds new record to terrorists indices catalog.
     *
     * @param string $name
     * @return bool
     * @throws \Exception
     */
    public function addTerroristsIndexRecord(string $name)
    {
        $index = $this->elasticaClient->getIndex($this->name);

        if (!$index->exists()) {
            throw new \Exception("Terrorist indices index should be created first.");
        }

        $currentDT = new MicroDateTime();
        $elasticCurrentDT = $currentDT->format('Y-m-d') . "T" . $currentDT->format('H:m:s.u');

        $type = $index->getType($this->type);
        $type->addDocument(new ElasticaDocument('', [
            'indexName' => $name,
            'indexDate' => $elasticCurrentDT
        ]));

        $index->refresh();

        return true;
    }

    /**
     * Removes all records about terrorist indices
     * except latest two. Returns an array with
     * names of indices, records about which were removed.
     *
     * @return array
     */
    public function removeUnusedTerroristsIndexRecords()
    {
        $search = new Search($this->elasticaClient);
        $search->addIndex($this->name);
        $search->addType($this->type);

        $query = new Query([
            'query' => [
                'match_all' => (object)[]
            ],
            'sort' => [
                'indexDate' => ['order' => 'desc']
            ]
        ]);

        $search->setQuery($query);
        $resultSet = $search->search();
        $results = $resultSet->getResults();

        $unusedTerroristsIndexNames = [];
        $documentsToDelete = [];

        for ($i = self::KEEP_TOP_N_INDICES; $i < count($results); $i++) {
            $result = $results[$i];

            $unusedTerroristsIndexNames[] = $result->getSource()['indexName'];
            $documentsToDelete[] = $result->getDocument();
        }

        $index = $this->elasticaClient->getIndex($this->name);
        $index->deleteDocuments($documentsToDelete);

        return $unusedTerroristsIndexNames;
    }

    /**
     * Deletes terrorists indices catalog index from Elasticsearch instance.
     *
     * @return bool
     */
    public function delete()
    {
        $this->elasticaClient->getIndex($this->name)->delete();

        return true;
    }
}