<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 06/02/2019
 * Time: 19:54
 */

namespace App\BusinessCase\TerroristsSearches;

/**
 * Interface ByIdSearchInterface
 * @package App\BusinessCase\TerroristsSearches
 */
interface ByIdSearchInterface extends SearchPagerInteface
{
    /**
     * Returns terrorist catalog item representation by it's listNumber/id.
     *
     * @param array $searchParams
     * @return mixed
     */
    public function search(array $searchParams);
}