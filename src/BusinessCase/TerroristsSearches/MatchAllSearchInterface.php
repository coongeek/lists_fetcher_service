<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 06/02/2019
 * Time: 15:46
 */

namespace App\BusinessCase\TerroristsSearches;

/**
 * Interface MatchAllSearchInterface
 * @package App\BusinessCase\TerroristsSearches
 */
interface MatchAllSearchInterface extends SearchPagerInteface
{
    /**
     * Returns terrorist catalog representation.
     *
     * @param array $searchParameters
     * @return mixed
     */
    public function search(array $searchParameters);
}