<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 06/02/2019
 * Time: 19:09
 */

namespace App\BusinessCase\TerroristsSearches;


use Symfony\Component\HttpFoundation\Request;

/**
 * Interface SearchPagerInteface
 * @package App\BusinessCase\TerroristsSearches
 */
interface SearchPagerInteface
{
    /**
     * Returns paging for last performed search.
     *
     * @param Request $request
     * @return mixed
     */
    public function getLastSearchPaging(Request $request);
    public function search(array $params);
}