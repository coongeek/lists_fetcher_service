<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 06/02/2019
 * Time: 19:21
 */

namespace App\BusinessCase\TerroristsSearches;

/**
 * Interface ByFullNameSearchInterface returns representation
 * of terrorist catalog filtered by specified fullName.
 * @package App\BusinessCase\TerroristsSearches
 */
interface ByFullNameSearchInterface extends SearchPagerInteface
{
    /**
     * Returns representation of terrorist catalog
     * filtered by specified fullName.
     *
     * @param array $searchParams
     * @return mixed
     */
    public function search(array $searchParams);
}