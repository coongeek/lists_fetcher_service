<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 05/01/2019
 * Time: 23:12
 */

namespace App\BusinessCase\Foundation\DOMParserResultProcessor;

/**
 * Interface DOMParserInterface
 *
 * @package App\BusinessCase\Foundation\DOMParserResultProcessor
 * @author Valentin Kniazev <coongeek@gmail.com>
 */
interface DOMParserInterface
{
    /**
     * Processes given DOM.
     *
     * @param string $dom
     * @return mixed
     */
    public function parse(string $dom);

    /**
     * Returns errors occurred during parsing.
     *
     * @return mixed
     */
    public function getErrors();
}