<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 05/01/2019
 * Time: 23:08
 */

namespace App\BusinessCase\Foundation\DOMParserResultProcessor;


interface DOMLoaderInterface
{
    /**
     * Loads string containing DOM.
     *
     * @return string
     */
    public function load();
}