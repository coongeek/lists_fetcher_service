<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 05/01/2019
 * Time: 23:14
 */

namespace App\BusinessCase\Foundation\DOMParserResultProcessor;

/**
 * Class DOMParserResultProcessor provides basic foundation
 * for processing parse results.
 *
 * @package App\BusinessCase\SimpleHttpDOMParser
 */
abstract class DOMParserResultProcessor implements DOMParserResultProcessorInterface
{
    protected $domLoader;
    protected $domParser;

    public function __construct(
        DOMLoaderInterface $domLoader,
        DOMParserInterface $domParser
    ) {
        $this->domLoader = $domLoader;
        $this->domParser = $domParser;
    }

    /**
     * Loads and parses DOM, providing parsing results
     * that could be used by process() method implementation.
     *
     * @return mixed
     */
    protected function getParseResults()
    {
        $dom = $this->domLoader->load();
        return $this->domParser->parse($dom);
    }

    abstract function process();
}