<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 05/01/2019
 * Time: 23:38
 */

namespace App\BusinessCase\Foundation\DOMParserResultProcessor;


interface DOMParserResultProcessorInterface
{
    /**
     * Perform processing of DOM parsing results.
     *
     * @return mixed
     */
    public function process();
}