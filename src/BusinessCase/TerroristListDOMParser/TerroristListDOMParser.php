<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 06/01/2019
 * Time: 11:58
 */

namespace App\BusinessCase\TerroristListDOMParser;


use App\BusinessCase\Foundation\DOMParserResultProcessor\DOMParserInterface;
use App\BusinessCase\TerroristConverter\TerroristConverterInterface;
use App\BusinessCase\TerroristListDOMParser\Event\TerroristParsedEvent;
use App\BusinessCase\TerroristListDOMParser\Event\TerroristParseErrorEvent;
use Sunra\PhpSimple\HtmlDomParser;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class TerroristListDOMParser parses given
 * terrorist list DOM into array of terrorist catalog item
 * representations which concrete type is specified by wired
 * TerroristConverterInterface instance.
 *
 * @package App\BusinessCase\TerroristListDOMParser
 * @author Valentin Kniazev <coongeek@gmail.com>
 */
class TerroristListDOMParser implements DOMParserInterface
{
    protected $parseErrors;

    protected $eventDispatcher;
    protected $terroristConverter;
    protected $maxFileSize;
    protected $memoryLimit;
    protected $timeLimit;

    public function __construct(
        TerroristConverterInterface $terroristConverter,
        EventDispatcherInterface $eventDispatcher = null,
        $maxFileSize = 10000000,
        $memoryLimit = '1G',
        $timeLimit = null)
    {
        $this->terroristConverter = $terroristConverter;
        $this->eventDispatcher = $eventDispatcher;
        $this->maxFileSize = $maxFileSize;
        $this->memoryLimit = $memoryLimit;
        $this->timeLimit = $timeLimit;
    }

    /**
     * Parse terrorist list DOM-containing string into
     * array of terrorist representations.
     *
     * @param string $dom
     * @return array|mixed
     */
    public function parse(string $dom)
    {
        if (!is_null($this->maxFileSize) && !defined('MAX_FILE_SIZE')) {
            define('MAX_FILE_SIZE', $this->maxFileSize);
        }

        if (!is_null($this->memoryLimit)) {
            ini_set('memory_limit', $this->memoryLimit);
        }

        if (!is_null($this->timeLimit)) {
            set_time_limit($this->timeLimit);
        }

        $htmlDOM = HtmlDomParser::str_get_html($dom);
        $terroristListItems = $htmlDOM->find('#russianFL ol.terrorist-list li');

        $terrorists = [];
        $this->parseErrors = [];

        foreach ($terroristListItems as $item) {
            try {
                $terroristStr = $item->text();
                $terrorist = $this->terroristConverter->convert($terroristStr);

                $terrorists[] = $terrorist;

                if (!is_null($this->eventDispatcher)) {
                    $this->eventDispatcher->dispatch(TerroristParsedEvent::NAME, new TerroristParsedEvent($terrorist));
                }
            } catch (\Exception $ex) {
                $this->parseErrors[] = $ex;

                if (!is_null($this->eventDispatcher)) {
                    $this->eventDispatcher->dispatch(TerroristParseErrorEvent::NAME, new TerroristParseErrorEvent($ex));
                }
            }
        }

        return $terrorists;
    }

    /**
     * Returns array of errors occurred during parsing terrorist list items.
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->parseErrors;
    }
}