<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 09/01/2019
 * Time: 10:48
 */

namespace App\BusinessCase\TerroristListDOMParser\Event;


use Symfony\Component\EventDispatcher\Event;

/**
 * This event signals about error occurred during parsing
 * of single terrorist representation.
 *
 * @package App\BusinessCase\TerroristListDOMParser\Event
 * @author Valentin Kniazev <coongeek@gmail.com>
 */
class TerroristParseErrorEvent extends Event
{
    const NAME = 'terrorist.parse_error';

    protected $error;

    public function __construct(\Exception $error)
    {
        $this->error = $error;
    }

    /**
     * @return \Exception
     */
    public function getError()
    {
        return $this->error;
    }
}