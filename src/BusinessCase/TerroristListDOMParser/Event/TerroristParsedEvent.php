<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 08/01/2019
 * Time: 11:26
 */

namespace App\BusinessCase\TerroristListDOMParser\Event;


use Symfony\Component\EventDispatcher\Event;

/**
 * This event signals about successful parsing of single terrorist representation.
 *
 * @package App\BusinessCase\TerroristListDOMParser\Event
 * @author Valentin Kniazev <coongeek@gmail.com>
 */
class TerroristParsedEvent extends Event
{
    const NAME = 'terrorist.parsed';

    protected $terrorist;

    public function __construct($terrorist)
    {
        $this->terrorist = $terrorist;
    }

    public function getTerrorist()
    {
        return $this->terrorist;
    }
}