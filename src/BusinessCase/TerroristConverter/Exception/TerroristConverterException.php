<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 09/01/2019
 * Time: 15:48
 */

namespace App\BusinessCase\TerroristConverter\Exception;

/**
 * Class TerroristConverterException
 *
 * @package App\BusinessCase\TerroristConverter\Exception
 * @author Valentin Kniazev <coongeek@gmail.com>
 */
class TerroristConverterException extends \Exception {}