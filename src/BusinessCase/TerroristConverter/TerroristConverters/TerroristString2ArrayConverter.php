<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 09/01/2019
 * Time: 12:49
 */

namespace App\BusinessCase\TerroristConverter\TerroristConverters;


use App\BusinessCase\TerroristConverter\Exception\TerroristConverterException;
use App\BusinessCase\TerroristConverter\TerroristConverterInterface;

/**
 * Implementation of TerroristConverterInterface which transforms terrorist
 * string into terrorist array.
 *
 * @package App\BusinessCase\TerroristConverter\TerroristConverters
 * @author Valentin Kniazev <coongeek@gmail.com>
 */
class TerroristString2ArrayConverter implements TerroristConverterInterface
{
    const TERRORIST_STR_FULL_NAME = 0;
    const TERRORIST_STR_ALT_NAME = 1;
    const TERRORIST_STR_BIRTH_DATE = 2;
    const TERRORIST_STR_LIST_NUMBER = 3;
    const TERRORIST_STR_ADDRESS = 4;
    const TERRORIST_STR_NODE_UNKNOWN = 5;
    const TERRORIST_STR_EMPTY_NODE = 6;

    /**
     * Converts given string representation of terrorist to array.
     *
     * @param $terrorist
     * @return array|mixed
     * @throws TerroristConverterException
     */
    public function convert($terrorist)
    {
        return $this->getTerroristArray($terrorist);
    }

    /**
     * Extracts nodes of source string building scheme of the string in the process,
     * then maps meaningful nodes to elements of terrorist array, returning it as
     * the result.
     *
     * @param string $terroristStr
     * @return array
     * @throws TerroristConverterException
     */
    protected function getTerroristArray(string $terroristStr)
    {
        $terroristStrCopy = $terroristStr;

        $terroristStrCopy = preg_replace('#\*#', '', $terroristStrCopy);
        $terroristStrCopy = preg_replace('#\sг.р.#', '', $terroristStrCopy);

        $nodes = [];
        $nodesProcessed = 0;
        $terroristStrScheme = '';
        $isLastNode = false;

        while (!$isLastNode) {
            if (count($nodes) === 0) {
                $delimeter = '.';
            } else {
                $delimeter = ',';
            }

            $meetsLastNodeRequirements = mb_strpos($terroristStrCopy, ',') === false;

            if ($meetsLastNodeRequirements) {
                $isLastNode = true;

                if (mb_substr($terroristStrCopy, -1, 1) === ';') {
                    $terroristStrCopy = mb_substr($terroristStrCopy, 0, mb_strlen($terroristStrCopy) -  1);
                }

                $node = trim($terroristStrCopy);
            } else {
                $node = trim(mb_strstr($terroristStrCopy, $delimeter, true));
                $terroristStrCopy = mb_substr(mb_strstr($terroristStrCopy, $delimeter), 1);
            }

            $isFullNameNodeExpected = $nodesProcessed === 1;

            if ($isFullNameNodeExpected) {
                $node = preg_replace('#[\s]?-[\s]?#', '-', $node);
            }

            $nodeType = $this->determineNodeType($node, $nodesProcessed);

            $terroristStrScheme .= $nodeType;

            $nodes[] = $node;
            $nodesProcessed++;
        }

        if (strpos($terroristStrScheme, '0') !== 1) {
            throw new TerroristConverterException("'Could not recognize full name node of terrorist list item': {$terroristStr} ({$terroristStrScheme})");
        }

        $map = [
            self::TERRORIST_STR_FULL_NAME => 'fullName',
            self::TERRORIST_STR_ALT_NAME => 'altName',
            self::TERRORIST_STR_BIRTH_DATE => 'birthDate',
            self::TERRORIST_STR_LIST_NUMBER => 'listNumber',
            self::TERRORIST_STR_ADDRESS => 'address'
        ];

        $terrorist = [
            'originalItem' => $terroristStr,
        ];

        $terroristStrSchemeNodes = str_split($terroristStrScheme);

        for ($i = 0; $i < count($terroristStrSchemeNodes); $i++) {
            $terroristStrSchemeNode = $terroristStrSchemeNodes[$i];

            if (!array_key_exists($terroristStrSchemeNode, $map)) {
                continue;
            }

            $terroristField = $map[$terroristStrSchemeNode];

            if ($terroristStrSchemeNode == self::TERRORIST_STR_ADDRESS) {
                if (!array_key_exists($terroristField, $terrorist)) {
                    $terrorist[$terroristField] = $nodes[$i];
                } else {
                    $terrorist[$terroristField] .= ', ' . $nodes[$i];
                }
            } else if ($terroristStrSchemeNode == self::TERRORIST_STR_BIRTH_DATE) {
                $terrorist[$terroristField] = \DateTime::createFromFormat('d.m.Y H:i:s', $nodes[$i] . ' 00:00:00');
            } else {
                $terrorist[$terroristField] = $nodes[$i];
            }
        }

        return $terrorist;
    }

    /**
     * Determines type(types are represented by current class constants
     * with TERRORIST_STR_ prefix) of the given terrorist string node
     * taking it's position at the source string into account.
     *
     * @param string $node
     * @param int $nodeIndex
     * @return int|string
     */
    protected function determineNodeType(string $node, int $nodeIndex)
    {
        $listNumberNodePattern = '#^[1-9]{1}[0-9]*$#';//+
        $fullNameNodePattern = '#^[А-ЯЁ]+(-[А-ЯЁ]+){0,}(\s([А-ЯЁ]+(-[А-ЯЁ]+){0,})){0,}#';
        $altNameNodePattern = '#^\([А-ЯЁ]+(-[А-ЯЁ]+){0,}(\s([А-ЯЁ]+(-[А-ЯЁ]+){0,})){0,}(;[\s]*[А-ЯЁ]+(-[А-ЯЁ]+){0,}(\s([А-ЯЁ]+(-[А-ЯЁ]+){0,})){0,})*\)$#';
        $birthDateNodePattern = '#([0]{1}[1-9]{1}|[12]{1}[0-9]{1}|[3]{1}[01]{1}).([0]{1}[1-9]{1}|[1]{1}[0-2]{1}).(19[0-9]{2}|20[0-9]{2})#';
        $emptyNodePattern = '#^([;]?|)$#';

        if ($nodeIndex === 0) {
            if (preg_match($listNumberNodePattern, $node) === 1) {
                return self::TERRORIST_STR_LIST_NUMBER;
            }
        } else if ($nodeIndex === 1) {
            if (preg_match($fullNameNodePattern, $node) === 1) {
                return self::TERRORIST_STR_FULL_NAME;
            }
        } else {
            $map = [
                self::TERRORIST_STR_ALT_NAME => $altNameNodePattern,
                self::TERRORIST_STR_BIRTH_DATE => $birthDateNodePattern,
                self::TERRORIST_STR_EMPTY_NODE => $emptyNodePattern,
            ];

            foreach ($map as $nodeType => $regexPattern) {
                if (preg_match($regexPattern, $node) === 1) {
                    return $nodeType;
                }
            }
        }

        return self::TERRORIST_STR_ADDRESS;
    }
}