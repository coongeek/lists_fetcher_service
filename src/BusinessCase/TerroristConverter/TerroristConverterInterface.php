<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 09/01/2019
 * Time: 12:50
 */

namespace App\BusinessCase\TerroristConverter;

/**
 * TerroristConverterInterface
 *
 * @package App\BusinessCase\TerroristConverter
 * @author Valentin Kniazev <coongeek@gmail.com>
 */
interface TerroristConverterInterface
{
    /**
     * Creates new representation of terrorist entity.
     *
     * @param $terrorist
     * @return mixed
     */
    public function convert($terrorist);
}