<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 11/02/2019
 * Time: 20:55
 */

namespace App\BusinessCase;


use Throwable;

class ParameterException extends \Exception {
    protected $parameterName;

    public function __construct(string $parameterName, string $message = "", int $code = 0, Throwable $previous = null)
    {
        $this->parameterName = $parameterName;
        parent::__construct($message, $code, $previous);
    }

    public function getParameterName()
    {
        return $this->parameterName;
    }
}