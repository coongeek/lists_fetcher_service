<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 05/02/2019
 * Time: 20:37
 */

namespace App\BusinessCase\TerroristListActualizer\Events;


use Symfony\Component\EventDispatcher\Event;

class TerroristsIndicesCatalogUnusedRecordsRemoved extends Event
{
    const NAME = 'terrorists_indices_catalog.unused_records_removed';

    protected $removedUnusedRecords;
    protected $date;

    public function __construct(array $removedUnusedRecords)
    {
        $this->removedUnusedRecords = $removedUnusedRecords;
        $this->date = new \DateTime();
    }

    public function getRemovedUnusedRecords()
    {
        return $this->removedUnusedRecords;
    }

    public function getDate()
    {
        return $this->date;
    }
}