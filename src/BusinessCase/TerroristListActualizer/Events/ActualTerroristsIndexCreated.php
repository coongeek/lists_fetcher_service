<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 05/02/2019
 * Time: 19:49
 */

namespace App\BusinessCase\TerroristListActualizer\Events;


use Symfony\Component\EventDispatcher\Event;

class ActualTerroristsIndexCreated extends Event
{
    const NAME = 'actual_terrorists_index.created';

    protected $name;
    protected $type;
    protected $terroristsAdded;
    protected $date;

    public function __construct(array $terroristsAdded, $name, $type = null)
    {
        $this->name = $name;
        $this->type = $type;
        $this->terroristsAdded = $terroristsAdded;
        $this->date = new \DateTime();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getAddedTerrorists()
    {
        return $this->terroristsAdded;
    }

    public function getDate()
    {
        return $this->date;
    }
}