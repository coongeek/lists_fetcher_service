<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 05/02/2019
 * Time: 20:25
 */

namespace App\BusinessCase\TerroristListActualizer\Events;


use Symfony\Component\EventDispatcher\Event;

class TerroristsIndicesCatalogRecordCreatedEvent extends Event
{
    const NAME = 'terrorists_indices_catalog.record_created';

    protected $indexName;
    protected $date;

    public function __construct(string $indexName)
    {
        $this->indexName = $indexName;
        $this->date = new \DateTime();
    }

    public function getIndexName()
    {
        return $this->indexName;
    }

    public function getDate()
    {
        return $this->date;
    }
}