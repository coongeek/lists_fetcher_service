<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 05/02/2019
 * Time: 19:35
 */

namespace App\BusinessCase\TerroristListActualizer\Events;


use Symfony\Component\EventDispatcher\Event;

class TerroristIndicesCatalogCreatedEvent extends Event
{
    const NAME = 'terrorist_indices_catalog.created';

    protected $name;
    protected $type;
    protected $date;

    //TODO: make type non-mandatory for all other classes
    public function __construct($name, $type = null)
    {
        $this->name = $name;
        $this->type = $type;
        $this->date = new \DateTime();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getDate()
    {
        return $this->date;
    }
}