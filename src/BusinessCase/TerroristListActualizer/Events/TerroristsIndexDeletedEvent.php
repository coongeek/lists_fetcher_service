<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 05/02/2019
 * Time: 20:57
 */

namespace App\BusinessCase\TerroristListActualizer\Events;


use Symfony\Component\EventDispatcher\Event;

class TerroristsIndexDeletedEvent extends Event
{
    const NAME = 'terrorists_index.deleted';

    protected $date;
    protected $name;

    public function __construct($name)
    {
        $this->name = $name;
        $this->date = new \DateTime();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDate()
    {
        return $this->date;
    }
}