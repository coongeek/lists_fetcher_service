<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 06/01/2019
 * Time: 16:13
 */

namespace App\BusinessCase\TerroristListActualizer;


use App\BusinessCase\Foundation\DOMParserResultProcessor\DOMLoaderInterface;
use App\BusinessCase\Foundation\DOMParserResultProcessor\DOMParserInterface;
use App\BusinessCase\Foundation\DOMParserResultProcessor\DOMParserResultProcessor;
use App\BusinessCase\TerroristListActualizer\Events\ActualTerroristsIndexCreated;
use App\BusinessCase\TerroristListActualizer\Events\TerroristIndicesCatalogCreatedEvent;
use App\BusinessCase\TerroristListActualizer\Events\TerroristsIndexDeletedEvent;
use App\BusinessCase\TerroristListActualizer\Events\TerroristsIndicesCatalogRecordCreatedEvent;
use App\BusinessCase\TerroristListActualizer\Events\TerroristsIndicesCatalogUnusedRecordsRemoved;
use App\BusinessCase\TerroristsIndex;
use App\BusinessCase\TerroristsIndicesIndex;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class TerroristListActualizer creates ElasticSearch index containing most recent terrorist catalog
 * loaded by DOMLoaderInterface and parsed into terrorist catalog items representation by DOMParserInterface.
 * Name of created index is stored in terrorists indices catalog index(in same ElasticSearch instance)
 * to ease the process of obtaining most actual terrorist index name to search in and to maintain service
 * availability during terrorist list catalog update.
 *
 * @package App\BusinessCase\TerroristListActualizer
 */
class TerroristListActualizer extends DOMParserResultProcessor
{
    protected $terroristRepository;
    protected $terroristIndicesIndex;
    /** @var TerroristsIndex */
    protected $terroristsIndex;
    protected $eventDispatcher;


    public function __construct(
        DOMLoaderInterface $domLoader,
        DOMParserInterface $domParser,
        TerroristsIndicesIndex $terroristsIndicesIndex,
        TerroristsIndex $terroristsIndex,
        EventDispatcherInterface $eventDispatcher = null
    ) {
        parent::__construct($domLoader, $domParser);
        $this->terroristRepository = $terroristsIndex;
        $this->terroristIndicesIndex = $terroristsIndicesIndex;
        $this->terroristsIndex = $terroristsIndex;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @return mixed
     */
    public function process()
    {
        $terrorists = $this->getParseResults();

        if (!$this->terroristIndicesIndex->exists()) {
            $this->terroristIndicesIndex->create();

            if ($this->eventDispatcher instanceof EventDispatcherInterface) {
                $this->eventDispatcher->dispatch(
                    TerroristIndicesCatalogCreatedEvent::NAME,
                    new TerroristIndicesCatalogCreatedEvent(
                        $this->terroristIndicesIndex->getName(),
                        $this->terroristIndicesIndex->getType()
                    )
                );
            }
        }

        $actualIndexName = $this->terroristsIndex->createActual($terrorists);

        if ($this->eventDispatcher instanceof EventDispatcherInterface) {
            $this->eventDispatcher->dispatch(
                ActualTerroristsIndexCreated::NAME,
                new ActualTerroristsIndexCreated(
                    $terrorists,
                    $actualIndexName,
                    $this->terroristsIndex->getType()
                )
            );
        }

        $this->terroristIndicesIndex->addTerroristsIndexRecord($actualIndexName);

        if ($this->eventDispatcher instanceof EventDispatcherInterface) {
            $this->eventDispatcher->dispatch(
                TerroristsIndicesCatalogRecordCreatedEvent::NAME,
                new TerroristsIndicesCatalogRecordCreatedEvent($actualIndexName)
            );
        }

        $unusedTerroristIndexNames = $this->terroristIndicesIndex
            ->removeUnusedTerroristsIndexRecords();

        if (count($unusedTerroristIndexNames) > 0) {
            if ($this->eventDispatcher instanceof EventDispatcherInterface) {
                $this->eventDispatcher->dispatch(
                    TerroristsIndicesCatalogUnusedRecordsRemoved::NAME,
                    new TerroristsIndicesCatalogUnusedRecordsRemoved($unusedTerroristIndexNames)
                );
            }
        }

        foreach ($unusedTerroristIndexNames as $indexName) {
            $this->terroristsIndex->deleteByName($indexName);

            if ($this->eventDispatcher instanceof EventDispatcherInterface) {
                $this->eventDispatcher->dispatch(
                    TerroristsIndexDeletedEvent::NAME,
                    new TerroristsIndexDeletedEvent($indexName)
                );
            }
        }

        return true;
    }
}