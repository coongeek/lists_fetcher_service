<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 10/02/2019
 * Time: 14:34
 */

namespace App\BusinessCase;


use Elastica\Client;
use Elastica\Search;
use Elastica\Query;

/**
 * Class ActualTerroristsIndex provides access to
 * most recent and actual terrorist index.
 *
 * @package App\BusinessCase
 */
class ActualTerroristsIndex
{
    protected $elasticaClient;
    protected $terroristIndicesCatalogName;
    protected $terroristIndicesCatalogType;
    protected $terroristIndexType;

    public function __construct(
        Client $elasticaClient,
        string $terroristIndicesCatalogName,
        string $terroristIndicesCatalogType = null,
        string $terroristIndexType = null
    ) {
        $this->elasticaClient = $elasticaClient;
        $this->terroristIndicesCatalogName = $terroristIndicesCatalogName;
        $this->terroristIndicesCatalogType = $terroristIndicesCatalogType;
        $this->terroristIndexType = $terroristIndexType;
    }

    /**
     * Returns terrorist index name from latest
     * terrorists indices catalog record.
     *
     * @return bool
     */
    public function getName()
    {
        $search = new Search($this->elasticaClient);
        $search->addIndex($this->terroristIndicesCatalogName);
        if (!empty($this->terroristIndicesCatalogType)) {
            $search->addType($this->terroristIndicesCatalogType);
        }

        $query = new Query([
            'query' => [
                'match_all' => (object)[]
            ],
            'sort' => [
                'indexDate' => ['order' => 'desc']
            ],
            'size' => 1
        ]);

        $search->setQuery($query);
        $resultSet = $search->search();

        $results = $resultSet->getResults();

        if (count($results) === 0) {
            return false;
        }

        return $results[0]->getSource()['indexName'];
    }

    public function getType()
    {
        return $this->terroristIndexType;
    }
}