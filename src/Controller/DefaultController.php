<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 06/02/2019
 * Time: 23:14
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController
{
    public function index()
    {
        return $this->redirectToRoute('app.swagger_ui');
    }
}