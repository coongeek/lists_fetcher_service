<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 11/02/2019
 * Time: 19:12
 */

namespace App\Controller\Utility;


class JSend
{
    const STATUS_SUCCESS = 'success';
    const STATUS_FAIL = 'fail';
    const STATUS_ERROR = 'error';
}