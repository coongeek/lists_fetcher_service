<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 14/01/2019
 * Time: 15:32
 */

namespace App\Controller;


use App\BusinessCase\ElasticaSearch\TerroristListSearch;
use App\BusinessCase\TerroristsSearches\ByFullNameSearchInterface;
use App\BusinessCase\TerroristsSearches\ByIdSearchInterface;
use App\BusinessCase\TerroristsSearches\MatchAllSearchInterface;
use App\Controller\Utility\JSend;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\RequestStack;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use App\Entity\Terrorist;

/**
 * Class TerroristsController
 * @package App\Controller
 */
class TerroristsController extends AbstractFOSRestController
{
    protected $requestStack;
    protected $paramFetcher;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @Rest\QueryParam(name="page", requirements="[0-9]+", strict=true, nullable=true, default="0", description="Page of terrorist list.")
     * @Rest\Get(path="/terrorists")
     * @SWG\Get(
     *     path="/terrorists",
     *     summary="Get terrorists list",
     *     description="Returns terrorist list which supports paging.",
     *     operationId="getTerrorists",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Optional. Terrorist list page number.",
     *         required=false,
     *         type="integer"
     *     )
     * )
     * @SWG\Response(
     *     response=200,
     *     description="OK",
     *     @SWG\Schema(
     *         @SWG\Property(
     *              property="status",
     *              type="string",
     *              example="success",
     *              default="success"
     *         ),
     *         @SWG\Property(
     *             property="data",
     *             type="object",
     *             @SWG\Property(
     *                  property="terrorists",
     *                  type="array",
     *                  maxItems=25,
     *                  @Model(type=Terrorist::class)
     *             ),
     *              @SWG\Property(
     *                  property="paging",
     *                  type="object",
     *                  @SWG\Property(
     *                      property="previous",
     *                      type="string",
     *                      x={"nullable": true}
     *                  ),
     *                  @SWG\Property(
     *                      property="next",
     *                      type="string",
     *                      x={"nullable": true}
     *                  )
     *              )
     *         )
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="status",
     *             type="string",
     *             example="fail",
     *             default="fail"
     *         ),
     *         @SWG\Property(
     *             property="data",
     *             type="object",
     *             @SWG\Property(
     *                 property="page",
     *                 type="string",
     *                 example="Page not found."
     *             )
     *         )
     *     )
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Internal server error.",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="status",
     *             type="string",
     *             example="error",
     *             default="error"
     *         ),
     *         @SWG\Property(
     *             property="message",
     *             type="string",
     *             example="Unexpected error occurred."
     *         )
     *     )
     * )
     */
    public function getTerroristsAction(ParamFetcher $paramFetcher, MatchAllSearchInterface $matchAllSearch)
    {
        $page = $paramFetcher->get('page');

        $params = ['page' => $page];
        $terrorists = $matchAllSearch->search($params);

        $data = [
            'terrorists' => $terrorists
        ];

        $paging = $matchAllSearch->getLastSearchPaging($this->requestStack->getMasterRequest());

        if (!empty($paging)) {
            $data['paging'] = $paging;
        }

        $res = [
            'status' => JSend::STATUS_SUCCESS,
            'data' => $data
        ];

        $view = $this->view($res, 200);

        return $this->handleView($view);
    }

    /**
     * @Rest\QueryParam(name="full_name", requirements="^[А-ЯЁа-яё]+(-[А-ЯЁа-яё]+){0,}(\s([А-ЯЁа-яё]+(-[А-ЯЁа-яё]+){0,})){0,}", strict=true, nullable=false)
     * @Rest\QueryParam(name="page", requirements="^[0-9]+$", strict=true, nullable=true, default="0")
     * @Rest\QueryParam(name="fuzziness", requirements="^[0-1]$", strict=true, nullable=true, default="0")
     * @Rest\Get("/terrorists/search")
     * @SWG\Get(
     *     path="/terrorists/search",
     *     summary="Search by terrorist name.",
     *     description="Search terrorist by exact full name match or get matches with up to 1 error.",
     *     operationId="searchTerrorists",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="full_name",
     *         pattern="^[А-ЯЁа-яё]+(-[А-ЯЁа-яё]+){0,}(\s([А-ЯЁа-яё]+(-[А-ЯЁа-яё]+){0,})){0,}",
     *         in="query",
     *         description="Mandatory. Full name of the terrorist.",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="fuzziness",
     *         pattern="^[0-1]$",
     *         in="query",
     *         description="Optional. Find more matches by including result with up to 1 symbol mismatch.",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="Optional. Results page number.",
     *         required=false,
     *         type="integer"
     *     )
     * )
     * @SWG\Response(
     *     response=200,
     *     description="OK",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="status",
     *             type="string",
     *             example="success"
     *         ),
     *         @SWG\Property(
     *             property="data",
     *             type="object",
     *             @SWG\Property(
     *                 property="terrorists",
     *                 type="array",
     *                 maxItems=25,
     *                 @Model(type=Terrorist::class)
     *             ),
     *             @SWG\Property(
     *                 property="paging",
     *                 type="object",
     *                 @SWG\Property(
     *                     property="previous",
     *                     type="string",
     *                     x={"nullable": true}
     *                 ),
     *                 @SWG\Property(
     *                     property="next",
     *                     type="string",
     *                     x={"nullable": true}
     *                 )
     *             )
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad request.",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="status",
     *             type="string",
     *             example="fail",
     *             default="fail"
     *         ),
     *         @SWG\Property(
     *             property="data",
     *             type="object",
     *             @SWG\Property(
     *                 property="parameterName",
     *                 type="string",
     *                 example="Invalid 'parameterName' parameter value."
     *             )
     *         )
     *     )
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Internal server error.",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="status",
     *             type="string",
     *             example="error",
     *             default="error"
     *         ),
     *         @SWG\Property(
     *             property="message",
     *             type="string",
     *             example="Unexpected error occurred."
     *         )
     *     )
     * )
     */
    public function searchTerroristsAction(ParamFetcher $paramFetcher, ByFullNameSearchInterface $byFullNameSearch)
    {
        $paramFetcher->getParams();
        $fullName = $paramFetcher->get('full_name');
        $page = $paramFetcher->get('page');
        $fuzziness = $paramFetcher->get('fuzziness');

        $terrorists = $byFullNameSearch->search([
            'fullName' => $fullName,
            'page' => $page,
            'fuzziness' => $fuzziness
        ]);

        $data = [
            'terrorists' => $terrorists,
        ];

        $paging = $byFullNameSearch->getLastSearchPaging($this->requestStack->getMasterRequest());

        if (!empty($paging)) {
            $data['paging'] = $paging;
        }

        $res = [
            'status' => 'success',
            'data' => $data
        ];

        $view = $this->view($res, 200);

        return $this->handleView($view);
    }

    /**
     * @Rest\Get(path="/terrorists/{id}", requirements={"id"="\d+"})
     * @SWG\Get(
     *     path="/terrorists/{id}",
     *     summary="Get terrorist by list number",
     *     description="Get terrorist by list number",
     *     produces={"application/json"},
     *     operationId="getTerroristById",
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="integer",
     *         description="Terrorist list number.",
     *     )
     * )
     * @SWG\Response(
     *     response=200,
     *     description="OK",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="status",
     *             type="string",
     *             example="success"
     *         ),
     *         @SWG\Property(
     *             property="data",
     *             type="object",
     *             @SWG\Property(
     *                 property="terrorist",
     *                 type="object",
     *                 ref="#/definitions/Terrorist"
     *             )
     *         )
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Bad request.",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="status",
     *             type="string",
     *             example="fail",
     *             default="fail"
     *         ),
     *         @SWG\Property(
     *             property="data",
     *             type="object",
     *             @SWG\Property(
     *                 property="parameterName",
     *                 type="string",
     *                 example="Invalid 'parameterName' parameter value."
     *             )
     *         )
     *     )
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Internal server error.",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="status",
     *             type="string",
     *             example="error",
     *             default="error"
     *         ),
     *         @SWG\Property(
     *             property="message",
     *             type="string",
     *             example="Unexpected error occurred."
     *         )
     *     )
     * )
     */
    public function getTerroristAction($id, ByIdSearchInterface $byIdSearch)
    {
        $terrorist = $byIdSearch->search(['id' => $id]);
        if (!empty($terrorist)) {
            $data = [
                'terrorist' => $terrorist
            ];
        } else {
            $data = null;
        }

        $res = [
            'status' => 'success',
            'data' => $data
        ];

        //what is there is more than 1 client with such an id?
        $view = $this->view($res, 200);

        return $this->handleView($view);
    }
}