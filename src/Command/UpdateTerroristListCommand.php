<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 06/01/2019
 * Time: 16:22
 */

namespace App\Command;


use App\BusinessCase\TerroristListActualizer\Events\ActualTerroristsIndexCreated;
use App\BusinessCase\TerroristListActualizer\Events\TerroristIndicesCatalogCreatedEvent;
use App\BusinessCase\TerroristListActualizer\Events\TerroristsIndexDeletedEvent;
use App\BusinessCase\TerroristListActualizer\Events\TerroristsIndicesCatalogRecordCreatedEvent;
use App\BusinessCase\TerroristListActualizer\Events\TerroristsIndicesCatalogUnusedRecordsRemoved;
use App\BusinessCase\TerroristListDOMParser\Event\TerroristParsedEvent;
use App\BusinessCase\TerroristListDOMParser\Event\TerroristParseErrorEvent;
use App\BusinessCase\TerroristConverter\Exception\TerroristConverterException;
use App\BusinessCase\TerroristListActualizer\TerroristListActualizer;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UpdateTerroristListCommand extends Command
{
    private $terroristListActualizer;
    private $eventDispatcher;
    private $logger;

    public function __construct(
        TerroristListActualizer $terroristListActualizer,
        EventDispatcherInterface $eventDispatcher,
        LoggerInterface $logger
    ) {
        $this->terroristListActualizer = $terroristListActualizer;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $logger;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:update-terrorist-list');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger->info('actualizer.started');

        $this->addConsoleListeners($output);
        $this->addLogListeners();

        $this->eventDispatcher->addListener(TerroristParsedEvent::NAME, function(TerroristParsedEvent $event) use($output) {
            $terrorist = $event->getTerrorist();
            $terrorist["birthDate"] = $terrorist["birthDate"]->format('d.m.Y');

            $output->writeln($terrorist);
            $output->writeln("======================");
        });

        $this->eventDispatcher->addListener(TerroristParseErrorEvent::NAME, function(TerroristParseErrorEvent $event) use($output) {
            $ex = $event->getError();

            if ($ex instanceof TerroristConverterException) {
                $output->writeln($ex->getMessage());
            } else {
                $output->writeln($ex->getMessage());
            }
        });

        try {
            $this->terroristListActualizer->process();
        } catch (\Exception $ex) {

            $output->writeln($ex->getMessage());
            $this->logger->critical("Message: " . $ex->getMessage() . " Stack Trace: " . $ex->getTraceAsString());
        }
    }

    protected function addConsoleListeners(OutputInterface $output)
    {
        $this->eventDispatcher->addListener(
            TerroristIndicesCatalogCreatedEvent::NAME,
            function(TerroristIndicesCatalogCreatedEvent $event) use ($output) {
                $catalogName = $event->getName();
                $eventDate = $event->getDate()->format('d.m.Y H:i:s');

                $outputMessage = "{$eventDate} Terrorists indices catalog created. Index name: '{$catalogName}'.";

                if (!is_null($event->getType())) {
                    $indexType = $event->getType();
                    $outputMessage .= " Index type: {$indexType}";
                }

                $output->writeln($outputMessage);
            }
        );

        $this->eventDispatcher->addListener(
            ActualTerroristsIndexCreated::NAME,
            function(ActualTerroristsIndexCreated $event) use ($output) {
                $eventDate = $event->getDate()->format('d.m.Y H:i:s');
                $indexName = $event->getName();
                $indexType = $event->getType();
                $addedTerrorists = $event->getAddedTerrorists();
                $addedTerroristsCount = count($addedTerrorists);

                $outputMessage = "{$eventDate} Actual terrorists index created."
                    . " Index name: {$indexName}.";

                if (!is_null($indexType)) {
                    $outputMessage .= " Index type: {$indexType}.";
                }

                $outputMessage .= " Records created: {$addedTerroristsCount}";

                $output->writeln($outputMessage);
            }
        );

        $this->eventDispatcher->addListener(
            TerroristsIndicesCatalogRecordCreatedEvent::NAME,
            function(TerroristsIndicesCatalogRecordCreatedEvent $event) use ($output) {
                $eventDate = $event->getDate()->format('d.m.Y H:i:s');
                $createdIndexName = $event->getIndexName();

                $outputMessage = "{$eventDate} Terrorists indices catalog populated with record "
                    . " about new index: {$createdIndexName}";
                $output->writeln($outputMessage);
            }
        );

        $this->eventDispatcher->addListener(
            TerroristsIndicesCatalogUnusedRecordsRemoved::NAME,
            function(TerroristsIndicesCatalogUnusedRecordsRemoved $event) use ($output) {
                $eventDate = $event->getDate()->format('d.m.Y H:i:s');

                $outputMessage = "{$eventDate} Records about following indices were"
                    . " removed from terrorists indices catalog: ";
                $output->writeln($outputMessage);

                foreach ($event->getRemovedUnusedRecords() as $indexName) {
                    $output->writeln($indexName);
                }
            }
        );

        $this->eventDispatcher->addListener(
            TerroristsIndexDeletedEvent::NAME,
            function(TerroristsIndexDeletedEvent $event) use ($output) {
                $eventDate = $event->getDate()->format('d.m.Y H:i:s');
                $indexName = $event->getName();

                $outputMessage = "{$eventDate} Following unused terrorists index was deleted: {$indexName}";
                $output->writeln($outputMessage);
            }
        );
    }

    protected function addLogListeners()
    {
        $this->eventDispatcher->addListener(
            TerroristIndicesCatalogCreatedEvent::NAME,
            function(TerroristIndicesCatalogCreatedEvent $event) {
                $logMessage = $event::NAME . " " . $event->getName() . " " . $event->getType();
                $this->logger->info($logMessage);
            }
        );

        $this->eventDispatcher->addListener(
            ActualTerroristsIndexCreated::NAME,
            function(ActualTerroristsIndexCreated $event) {
                $addedTerrorists = $event->getAddedTerrorists();
                $addedTerroristsCount = count($addedTerrorists);

                $logMessage = $event::NAME . " " . $event->getName() . " " . $event->getType() . " " . $addedTerroristsCount;
                $this->logger->info($logMessage);
            }
        );

        $this->eventDispatcher->addListener(
            TerroristsIndicesCatalogRecordCreatedEvent::NAME,
            function(TerroristsIndicesCatalogRecordCreatedEvent $event) {
                $logMessage = $event::NAME . " " . $event->getIndexName();
                $this->logger->info($logMessage);
            }
        );

        $this->eventDispatcher->addListener(
            TerroristsIndicesCatalogUnusedRecordsRemoved::NAME,
            function(TerroristsIndicesCatalogUnusedRecordsRemoved $event) {
                $logMessage = $event::NAME . " " . implode(",", $event->getRemovedUnusedRecords());
                $this->logger->info($logMessage);
            }
        );

        $this->eventDispatcher->addListener(
            TerroristsIndexDeletedEvent::NAME,
            function(TerroristsIndexDeletedEvent $event) {
                $logMessage = $event::NAME . " " . $event->getName();
                $this->logger->info($logMessage);
            }
        );
    }
}