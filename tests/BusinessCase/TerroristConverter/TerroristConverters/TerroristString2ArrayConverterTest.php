<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 09/01/2019
 * Time: 16:03
 */

namespace App\Tests\BusinessCase\TerroristConverter\TerroristConverters;


use App\BusinessCase\TerroristConverter\Exception\TerroristConverterException;
use App\BusinessCase\TerroristConverter\TerroristConverters\TerroristString2ArrayConverter;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class TerroristString2ArrayConverterTest extends TestCase
{
    protected function makeDefaultConverterInstance()
    {
        return new TerroristString2ArrayConverter();
    }

    /**
     * @test
     */
    public function convertValidTerroristStringTest()
    {
        $converter = $this->makeDefaultConverterInstance();

        $terrorist = $converter->convert('1. АБАДИЕВ МАГОМЕД-АХМЕД-ИБРЕК МИКАИЛОВИЧ ХАН*, 09.11.1982 г.р. , Г. НАЗРАНЬ ЧИАССР;');
        $this->assertSame('1', $terrorist['listNumber']);
        $this->assertSame('АБАДИЕВ МАГОМЕД-АХМЕД-ИБРЕК МИКАИЛОВИЧ ХАН', $terrorist['fullName']);
        $this->assertSame('09.11.1982', $terrorist['birthDate']->format('d.m.Y'));

        $terrorist = $converter->convert('8462. ШОДИЗОДА МУХАММАДИКБОЛ*, 09.02.1992 г.р. ;');
        $this->assertSame('8462', $terrorist['listNumber']);
        $this->assertSame('ШОДИЗОДА МУХАММАДИКБОЛ', $terrorist['fullName']);
        $this->assertSame('09.02.1992', $terrorist['birthDate']->format('d.m.Y'));

        $terroristStr = '7875. ХАТУЕВ НУР-МАГОМЕД ШАХИДОВИЧ*,  (ХАТУЕВ НУР-МАГОМЕД ШАХИДОВИЧ), 16.05.1967 г.р. , С. ГОЙТЫ УРУС-МАРТАНОВСКОГО РАЙОНА ЧИАССР;';
        $terrorist = $converter->convert($terroristStr);
        $this->assertSame('7875', $terrorist['listNumber']);
        $this->assertSame('ХАТУЕВ НУР-МАГОМЕД ШАХИДОВИЧ', $terrorist['fullName']);
        $this->assertSame('(ХАТУЕВ НУР-МАГОМЕД ШАХИДОВИЧ)', $terrorist['altName']);
        $this->assertSame('16.05.1967', $terrorist['birthDate']->format('d.m.Y'));

        $terroristStr = '6727. САФАРОВ ДУСТМУРОД РАЧАБАЛИЕВИЧ*,  (САФАРОВ ДУСТМУРОД РАДЖАБАЛИЕВИЧ; САФАРОВ ДУСТМУРОД РАЖАБАЛИЕВИЧ), 19.04.1983 г.р. , РЕСПУБЛИКА ТАДЖИКИСТАН;';
        $terrorist = $converter->convert($terroristStr);
        $this->assertSame('6727', $terrorist['listNumber']);
        $this->assertSame('САФАРОВ ДУСТМУРОД РАЧАБАЛИЕВИЧ', $terrorist['fullName']);
        $this->assertSame('(САФАРОВ ДУСТМУРОД РАДЖАБАЛИЕВИЧ; САФАРОВ ДУСТМУРОД РАЖАБАЛИЕВИЧ)', $terrorist['altName']);
        $this->assertSame('19.04.1983', $terrorist['birthDate']->format('d.m.Y'));

        $terroristStr = '46. АБДУЛЛАЕВ ИСЛАМ БЕЙДУЛЛАХОВИЧ*, 05.06.1997 г.р. , С. АВАДАН ДОКУЗПАРИНСКОГО РАЙОНА РЕСПУБЛИКИ ДАГЕСТАН, РОССИЙСКАЯ ФЕДЕРАЦИЯ, РЕСПУБЛИКА ДАГЕСТАН, ДОКУЗПАРИНСКИЙ РАЙОН, С. АВАДАН, ;';
        $terrorist = $converter->convert($terroristStr);
        $this->assertSame('46', $terrorist['listNumber']);
        $this->assertSame('АБДУЛЛАЕВ ИСЛАМ БЕЙДУЛЛАХОВИЧ', $terrorist['fullName']);
        $this->assertSame('05.06.1997', $terrorist['birthDate']->format('d.m.Y'));

        $terroristStr = '6727. САФАРОВ ДУСТМУРОД РАЧАБАЛИЕВИЧ*,  (САФАРОВ ДУСТМУРОД РАДЖАБАЛИЕВИЧ; САФАРОВ ДУСТМУРОД РАЖАБАЛИЕВИЧ);';
        $terrorist = $converter->convert($terroristStr);
        $this->assertSame('6727', $terrorist['listNumber']);
        $this->assertSame('САФАРОВ ДУСТМУРОД РАЧАБАЛИЕВИЧ', $terrorist['fullName']);
        $this->assertSame('(САФАРОВ ДУСТМУРОД РАДЖАБАЛИЕВИЧ; САФАРОВ ДУСТМУРОД РАЖАБАЛИЕВИЧ)', $terrorist['altName']);
    }

    /**
     * @test
     * @throws \App\BusinessCase\TerroristConverter\Exception\TerroristConverterException
     */
    public function convertInvalidTerroristStringTest()
    {
        $converter = $this->makeDefaultConverterInstance();

        $this->expectException(TerroristConverterException::class);

        $terroristStr = 'ДУСТМУРОД';
        $terrorist = $converter->convert($terroristStr);
    }
}