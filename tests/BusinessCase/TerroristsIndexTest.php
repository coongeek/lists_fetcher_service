<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 05/02/2019
 * Time: 15:51
 */

namespace App\Tests\BusinessCase;


use App\BusinessCase\TerroristsIndex;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TerroristsIndexTest extends KernelTestCase
{
    /** @var TerroristsIndex */
    protected $terroristsIndex;
    protected $lastCreatedIndexName;

    public function setUp()
    {
        $kernel = self::bootKernel();
        $this->terroristsIndex = $kernel->getContainer()->get(TerroristsIndex::class);
    }

    public function tearDown()
    {
        $this->terroristsIndex->deleteByName($this->lastCreatedIndexName);

        parent::tearDown();
    }

    public function testCreateActual()
    {
        $terrorists = [
            [
                'listNumber' => 1,
                'fullName' => 'Рахат Лукум',
                'birthDate' => new \DateTime('01.01.1961'),
                'original' => '1. Рахат Лукум, 01.01.1961'
            ]
        ];

        $createdIndexName = $this->terroristsIndex->createActual($terrorists);
        $this->lastCreatedIndexName = $createdIndexName;

        $this->assertTrue(mb_strlen($createdIndexName) > 0);
        $this->assertEquals(1, preg_match('#^(' . $this->terroristsIndex->getPrefix() . ')#', $createdIndexName));
    }
}