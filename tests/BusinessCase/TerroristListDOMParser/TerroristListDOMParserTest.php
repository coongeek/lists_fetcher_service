<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 11/01/2019
 * Time: 10:14
 */

namespace App\Tests\BusinessCase\TerroristListDOMParser;


use App\BusinessCase\TerroristConverter\TerroristConverters\TerroristString2ArrayConverter;
use App\BusinessCase\TerroristListDOMParser\Event\TerroristParsedEvent;
use App\BusinessCase\TerroristListDOMParser\Event\TerroristParseErrorEvent;
use App\BusinessCase\TerroristListDOMParser\TerroristListDOMParser;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class TerroristListDOMParserTest extends TestCase
{
    protected function makeEventDispatcherIstance()
    {
        return new EventDispatcher();
    }
    protected function makeParserInstance(EventDispatcherInterface $eventDispatcher = null)
    {
        $terroristConverter = new TerroristString2ArrayConverter();
        if (is_null($eventDispatcher)) {
            $eventDispatcher = new EventDispatcher();
        }

        $parser = new TerroristListDOMParser($terroristConverter, $eventDispatcher);

        return $parser;
    }

    protected function getInvalidTerroristListDOM()
    {
        return 'Clearly not DOM-containing string at all.';
    }

    protected function getValidTerroristListDOM()
    {
        //#russianFL ol.terrorist-list li
        return  '<html><head></head><body><div id="russianFL"><ol class="terrorist-list">' .
                '<li>2260. ГАЙРБЕКОВ СУЛИМ САИД - ЭМИНОВИЧ*, 17.08.1990 г.р. , Г. ГРОЗНЫЙ ЧИАССР;</li>' .
                '</ol></div></body></html>';
    }

    protected function getDOMWithValidAndInvalidTerroristString()
    {
        return  '<html><head></head><body><div id="russianFL"><ol class="terrorist-list">' .
            '<li>2260. ГАЙРБЕКОВ СУЛИМ САИД - ЭМИНОВИЧ*, 17.08.1990 г.р. , Г. ГРОЗНЫЙ ЧИАССР;</li>' .
            '<li>2261. сулим</li>' .
            '</ol></div></body></html>';
    }

    /**
     * @test
     */
    public function parseInvalidTerroristListDOMTest()
    {
        $parser = $this->makeParserInstance();

        $dom = $this->getInvalidTerroristListDOM();
        $parseResult = $parser->parse($dom);

        $this->assertEmpty($parseResult);
        $this->assertEmpty($parser->getErrors());
    }

    /**
     * @test
     */
    public function parseValidTerroristListDOMTest()
    {
        $parser = $this->makeParserInstance();

        $dom = $this->getValidTerroristListDOM();
        $parseResult = $parser->parse($dom);

        $this->assertCount(1, $parseResult);
    }

    /**
     * @test
     */
    public function parseDOMWithValidAndInvalidTerroristString()
    {
        $parser = $this->makeParserInstance();

        $dom = $this->getDOMWithValidAndInvalidTerroristString();
        $parseResult = $parser->parse($dom);
        $parseErrors = $parser->getErrors();

        $this->assertCount(1, $parseResult);
        $this->assertCount(1, $parseErrors);
    }

    /**
     * @test
     */
    public function terroristParsedEventOnParseSuccessTest()
    {
        $ed = $this->makeEventDispatcherIstance();
        $parser = $this->makeParserInstance($ed);

        $wasCalled = false;

        $ed->addListener(TerroristParsedEvent::NAME, function(TerroristParsedEvent $e) use(&$wasCalled) {
            $wasCalled = true;
        });

        $dom = $this->getValidTerroristListDOM();
        $parser->parse($dom);

        $this->assertTrue($wasCalled);
    }

    /**
     * @test
     */
    public function terroristParseErrorEventOnTerroristParseFailTest()
    {
        $ed = $this->makeEventDispatcherIstance();
        $parser = $this->makeParserInstance($ed);

        $wasCalled = false;

        $ed->addListener(TerroristParseErrorEvent::NAME, function(TerroristParseErrorEvent $e) use(&$wasCalled) {
            $wasCalled = true;
        });

        $dom = $this->getDOMWithValidAndInvalidTerroristString();
        $parser->parse($dom);

        $this->assertTrue($wasCalled);
    }
}