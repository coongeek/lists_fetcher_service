<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 04/02/2019
 * Time: 22:35
 */

namespace App\Tests\BusinessCase;


use App\BusinessCase\TerroristsIndicesIndex;
use Elastica\Client;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TerroristsIndicesIndexTest extends KernelTestCase
{
    /** @var TerroristsIndicesIndex */
    protected $terroristIndicesIndex;
    /** @var Client */
    protected $elasticaClient;

    public function setUp()
    {
        $kernel = self::bootKernel();
        $this->terroristIndicesIndex = $kernel->getContainer()->get(TerroristsIndicesIndex::class);
    }

    public function tearDown()
    {
        if ($this->terroristIndicesIndex->exists()) {
            $this->terroristIndicesIndex->delete();
        }
    }

    public function testExists_WithNonExistentIndex_ReturnsFalse()
    {
        $this->assertFalse($this->terroristIndicesIndex->exists());
    }

    public function testCreate_WithNonExistentIndex_IndexCreated()
    {
        $this->terroristIndicesIndex->create();

        $this->assertTrue($this->terroristIndicesIndex->exists());
    }

    public function testCreate_WithExistingIndex_ExceptionThrown()
    {
        $this->expectException(\Exception::class);

        $this->terroristIndicesIndex->create();
        $this->terroristIndicesIndex->create();
    }

    public function testAddTerroristsIndexRecord_WithExistingIndex_ReturnsTrue()
    {
        $this->terroristIndicesIndex->create();
        $result = $this->terroristIndicesIndex->addTerroristsIndexRecord('terrorists_1');

        $this->assertTrue($result);
    }

    public function testRemoveUnusedTerroristsIndexRecords_WithExistingRecords_ReturnsRemovedNames()
    {
        $this->terroristIndicesIndex->create();

        $this->terroristIndicesIndex->addTerroristsIndexRecord('terrorists_1');
        $this->terroristIndicesIndex->addTerroristsIndexRecord('terrorists_2');
        $this->terroristIndicesIndex->addTerroristsIndexRecord('terrorists_3');
        $this->terroristIndicesIndex->addTerroristsIndexRecord('terrorists_4');
        sleep(1);

        $unusedTerroristsIndexNames = $this->terroristIndicesIndex
            ->removeUnusedTerroristsIndexRecords();

        $this->assertCount(2, $unusedTerroristsIndexNames);
        $this->assertContains('terrorists_1', $unusedTerroristsIndexNames);
        $this->assertContains('terrorists_2', $unusedTerroristsIndexNames);
    }
}