<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 16/01/2019
 * Time: 11:36
 */

namespace App\Tests\Controller;


use App\BusinessCase\ActualTerroristsIndex;
use App\BusinessCase\TerroristsIndex;
use App\BusinessCase\TerroristsIndicesIndex;
use App\Controller\Utility\JSend;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TerroristsControllerTest extends WebTestCase
{
    protected $actualTerroristIndexName;

    public static function setUpBeforeClass()
    {
        $kernel = self::bootKernel();
        /** @var TerroristsIndex $terroristIndex */
        $terroristIndex = $kernel->getContainer()->get(TerroristsIndex::class);
        /** @var TerroristsIndicesIndex $terroristIndicesIndex */
        $terroristIndicesIndex = $kernel->getContainer()->get(TerroristsIndicesIndex::class);

        $actualIndexName = $terroristIndex->createActual($terrorists = [
            [
                'listNumber' => 1,
                'fullName' => 'Рахат Лукум',
                'birthDate' => new \DateTime('01.01.1961'),
                'original' => '1. Рахат Лукум, 01.01.1961'
            ]
        ]);

        $terroristIndicesIndex->create();
        $terroristIndicesIndex->addTerroristsIndexRecord($actualIndexName);
    }

    public static function tearDownAfterClass()
    {
        $kernel = self::bootKernel();
        /** @var ActualTerroristsIndex $actualTerroristIndex */
        $actualTerroristIndex = $kernel->getContainer()->get(ActualTerroristsIndex::class);
        /** @var TerroristsIndex $terroristIndex */
        $terroristIndex = $kernel->getContainer()->get(TerroristsIndex::class);
        /** @var TerroristsIndicesIndex $terroristIndicesIndex */
        $terroristIndicesIndex = $kernel->getContainer()->get(TerroristsIndicesIndex::class);

        $actualIndexName = $actualTerroristIndex->getName();
        $terroristIndex->deleteByName($actualIndexName);
        $terroristIndicesIndex->delete();
    }

    public function testGetTerroristsOK()
    {
        $client = static::createClient();

        $client->request('GET', '/terrorists', [], [], ['HTTP_ACCEPT' => 'application/json']);
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('application/json; charset=UTF-8', $response->headers->get('Content-Type'));

        $this->assertArrayHasKey('status', $content);
        $this->assertArrayHasKey('data', $content);
        $this->assertArrayHasKey('terrorists', $content['data']);

        $this->assertEquals(JSend::STATUS_SUCCESS, $content['status']);
        $this->assertCount(1, $content['data']);
        $this->assertEquals(
            [
                'listNumber' => 1,
                'fullName' => 'Рахат Лукум',
                'birthDate' => '1961-01-01',
                'original' => '1. Рахат Лукум, 01.01.1961'
            ],
            $content['data']['terrorists'][0]
        );
    }

    public function testGetTerroristsFail()
    {
        $client = static::createClient();

        $client->request('GET', '/terrorists', ['page' => 'test'], [], ['HTTP_ACCEPT' => 'application/json']);
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals('application/json; charset=UTF-8', $response->headers->get('Content-Type'));

        $this->assertArrayHasKey('status', $content);
        $this->assertArrayHasKey('data', $content);
        $this->assertArrayHasKey('page', $content['data']);

        $this->assertEquals(JSend::STATUS_FAIL, $content['status']);
    }

    public function testSearchTerroristsOK()
    {
        $client = static::createClient();

        $client->request(
            'GET',
            '/terrorists/search',
            array('full_name' => 'Рахат Лукум'),
            array(),
            array('HTTP_ACCEPT' => 'application/json')
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('application/json; charset=UTF-8', $response->headers->get('Content-Type'));

        $this->assertArrayHasKey('status', $content);
        $this->assertArrayHasKey('data', $content);
        $this->assertArrayHasKey('terrorists', $content['data']);

        $this->assertEquals(JSend::STATUS_SUCCESS, $content['status']);
        $this->assertCount(1, $content['data']['terrorists']);
        $this->assertEquals([
                'listNumber' => 1,
                'fullName' => 'Рахат Лукум',
                'birthDate' => '1961-01-01',
                'original' => '1. Рахат Лукум, 01.01.1961'
            ],
            $content['data']['terrorists'][0]
        );
    }

    public function testSearchTerroristsFail()
    {
        $client = static::createClient();

        $client->request(
            'GET',
            '/terrorists/search',
            ['full_name' => '1234'],
            [],
            ['HTTP_ACCEPT' => 'application/json']
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals('application/json; charset=UTF-8', $response->headers->get('Content-Type'));

        $this->assertArrayHasKey('status', $content);
        $this->assertArrayHasKey('data', $content);
        $this->assertArrayHasKey('full_name', $content['data']);

        $this->assertEquals(JSend::STATUS_FAIL, $content['status']);
    }

    public function testGetTerroristOK()
    {
        $client = static::createClient();

        $client->request(
            'GET',
            '/terrorists/1',
            [],
            [],
            ['HTTP_ACCEPT' => 'application/json']
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('application/json; charset=UTF-8', $response->headers->get('Content-Type'));

        $this->assertArrayHasKey('status', $content);
        $this->assertArrayHasKey('data', $content);
        $this->assertArrayHasKey('terrorist', $content['data']);

        $this->assertEquals(JSend::STATUS_SUCCESS, $content['status']);
        $this->assertEquals([
            'listNumber' => 1,
            'fullName' => 'Рахат Лукум',
            'birthDate' => '1961-01-01',
            'original' => '1. Рахат Лукум, 01.01.1961'
        ],
            $content['data']['terrorist']
        );
    }

    public function testGetTerrorist_nonExistantId_OK()
    {
        $client = static::createClient();

        $client->request(
            'GET',
            '/terrorists/2',
            [],
            [],
            ['HTTP_ACCEPT' => 'application/json']
        );
        $response = $client->getResponse();
        $content = json_decode($response->getContent(), true);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('application/json; charset=UTF-8', $response->headers->get('Content-Type'));

        $this->assertArrayHasKey('status', $content);
        $this->assertArrayHasKey('data', $content);

        $this->assertEquals(JSend::STATUS_SUCCESS, $content['status']);
        $this->assertEquals(null, $content['data']);
    }
}